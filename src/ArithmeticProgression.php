<?php


/**
 * Class ArithmeticProgression
 * Класс Арифметическая Прогрессия
 */
class ArithmeticProgression
{
    private $first = 0;
    private $step = 1;

    /**
     * ArithmeticProgression constructor.
     * @param $first
     * @param $step
     */
    public function __construct($first, $step)
    {
        $this->first = $first;
        $this->step = $step;
    }

    /**
     * Возвращает сумму первых n элементов прогрессии
     * @param $number
     * @return float|int
     */
    public function getAmount($number)
    {
        return ((2 * $this->first + $this->step * ($number - 1)) / 2) * $number;
    }

    /**
     * Возвращает n-ый элемент прогрессии
     * @param $number
     * @return float|int
     */
    public function getElement($number)
    {
        return $this->first + $this->step * ($number - 1);
    }
}